﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp142
{
   

public static class Pangram
{
    public static bool IsPangram(string input)
    {
        for(int i = 0; i <= input.Length; i++)
        {
            if(input.ToLower().Contains((char)i))
            {
                return false;
            }
        }
        return true;
    }
}
}
